# Desafio - JAVA

Olá dev! 

Te damos as boas vindas para nossa etapa de desafio técnico (olhar [Avaliação técnica - Interfacenet Java](avaliacao_tecnica_interfacenet_java.pdf)). Você utilizará esse repositório como base para seu projeto (via Fork). O mesmo já está pronto para receber suas implementações, permitindo você focar nos itens do desafio.

Boa sorte! :-)

## Instruções
- Desenvolver as novas funcionalidades de acordo com as instruções fornecidas pelo RH em arquivo PDF 
- Respeitar os padrões de arquitetura, nomenclatura e frameworks definidos da nossa stack
- Utilizar o fluxo do Git Flow para suas entregas

## Insights
- IDE Sugerida: VS Code
- Frameworks adicionais são de livre escolha
- Um repositório e um projeto bem documentados chamarão nossa atenção
- **Atenção para as configurações de banco de dados do projeto(sql server ou postgre)!**

## Requerimentos
* Java 11+
* PostgreSQL

# Execução do projeto
- Foi utilizada a IDE Intellij
- Necessário instalar o plugin do Lombok na IDE, e habilitar o 'Enable annotation processing'

## Descrição
Para detalhes sobre endpoints, consultar [API Doc](./src/main/resources/static/swagger.yml).
Por padrão pode-se acessar a URL http://localhost:8080/swagger-ui.html

## Variavéis de ambiente
* POSTGRE_URL - Atalho para `spring.datasource.url`
* POSTGRE_PORT - Atalho para `spring.datasource.url`
* POSTGRE_DB - Atalho para `spring.datasource.url`
* POSTGRE_USER - Atalho para `spring.datasource.username`
* POSTGRE_PASS - Atalho para `spring.datasource.password`
* SWAGGER_HOST - Variável opcional, host para o Swagger 2.0, formato da variável http://< URL >:< PORT >

### Docker

```bash
$ ./build.sh docker
```

### Docker Remote

```bash
docker run -d \
    <URL>:<PORT>/<PATH>/prova-java-api:TAG
```

### Agradecimentos e créditos
* O uso do pacote JJWT e a implementação do JWT foram 
com base no tutorial de JavaInUse, disponível em 
https://www.javainuse.com/webseries/spring-security-jwt/chap7,
acessado em 29/03/2021.