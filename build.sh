#!/usr/bin/env bash

mvn="./mvnw"
default_goals="clean compile test"
custom_args="${@:2}"

help()
{
    echo "****************************************************"
    echo "Comandos avaliados por agora"
    echo "docker : Executa os goals do maven para gerar e enviar as imagens do docker"
    echo "****************************************************"
}

docker()
{
    bash -c "$mvn $default_goals jib:build $custom_args"
}

case $1 in
"docker") docker ;;
*) help ;;
esac
