package br.com.alterdata.bimer.config;

import br.com.alterdata.bimer.prova.ProvaAlterdataApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.SwaggerResource;
import springfox.documentation.swagger.web.SwaggerResourcesProvider;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@Configuration
@EnableSwagger2
class SwaggerConfig {

    @Autowired
    private Environment env;

    @Bean
    public Docket api() {
        String hostUrl = env.getProperty("SWAGGER_HOST");
        Docket docket = new Docket(DocumentationType.SWAGGER_2).groupName("public-api");
        if(hostUrl != null && !hostUrl.isBlank()) docket=docket.host(hostUrl);
        return docket.select()
                     .apis(RequestHandlerSelectors.basePackage(ProvaAlterdataApplication.class
                                                                                        .getPackage()
                                                                                        .getName()))
                     //.paths(PathSelectors.ant("/api/**"))
                     .paths(PathSelectors.any())
                     .build()
                     .apiInfo(apiInfo());
    }

    @Bean
    @Primary
    public SwaggerResourcesProvider swaggerResourcesProvider(){
        return () -> {
            SwaggerResource swaggerResource = new SwaggerResource();
            swaggerResource.setName("Documentation");
            swaggerResource.setSwaggerVersion("2.0");
            swaggerResource.setLocation("/swagger.yml");
            return Collections.singletonList(swaggerResource);
        };
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder().title("Swagger API")
                                   .description("Documentação das APIs")
                                   .termsOfServiceUrl("http://127.0.0.1")
                                   .contact(new Contact("nome","url","any@gmail.com"))
                                   .license("License")
                                   .licenseUrl("any@gmail.com")
                                   .version("1.0").build();
    }

}