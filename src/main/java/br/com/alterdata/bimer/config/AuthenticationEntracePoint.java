package br.com.alterdata.bimer.config;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class AuthenticationEntracePoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request,
                         HttpServletResponse response,
                         AuthenticationException authException) throws IOException, ServletException {

        Exception exception =  (Exception) request.getAttribute("exception");

        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.getOutputStream()
                .write(new ObjectMapper().writeValueAsBytes(writeOutputStream((exception != null),
                                                                              exception,
                                                                              authException)));
    }

    private Map<String, String> writeOutputStream(Boolean hasException,
                                                  Exception exception,
                                                  AuthenticationException authException) {
        return hasException
                ? Collections.singletonMap("cause", exception.toString())
                : Collections.singletonMap("error", authExceptionMessage(authException));
    }

    private String authExceptionMessage(AuthenticationException authException) {
        return (authException.getCause() != null)
                    ? authException.getCause().toString() + " " + authException.getMessage()
                    : authException.getMessage();
    }

}
