package br.com.alterdata.bimer.config;

import br.com.alterdata.bimer.prova.servico.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserService userService;

    @Autowired
    private AuthenticationFilter authenticationFilter;

    @Autowired
    private AuthenticationEntracePoint authenticationEntracePoint;



    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }


    @Override
    public void configure(AuthenticationManagerBuilder authentication) throws Exception {
        authentication.userDetailsService(userService)
                      .passwordEncoder(passwordEncoder());
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception
    {
        return super.authenticationManagerBean();
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
//        String admin = RoleType.ROLE_ADMIN
//                               .getLabel()
//                               .replace("ROLE_","");
//        String user = RoleType.ROLE_USER
//                              .getLabel()
//                              .replace("ROLE_","");
        http.csrf()
            .disable()
            .authorizeRequests()
//                .antMatchers("/api/clients/search").hasRole(admin)
//                .antMatchers("/api/clients").hasAnyRole(user, admin)
            .antMatchers("/**").permitAll().anyRequest().authenticated()//TODO Por padrão todos os caminhos estão permitidos, no entanto pode ser colocado autenticação diretamente no método como @Secured, geralmente utilizado no controller.
            .and().exceptionHandling().authenticationEntryPoint(authenticationEntracePoint)
            .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            .and().addFilterBefore(authenticationFilter, UsernamePasswordAuthenticationFilter.class);
    }

}
