package br.com.alterdata.bimer.prova.exceptions.client;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "exception.dontHaveTwoDistinctContactType")
public class DontHaveTwoDistinctContactTypeException extends Exception {}