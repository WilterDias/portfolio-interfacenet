package br.com.alterdata.bimer.prova.modelo;

import br.com.alterdata.bimer.prova.dtos.user.UserDto;
import br.com.alterdata.bimer.prova.utils.ToStringUtils;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Entity
@Table(name = "userJwt")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
//@ToString(callSuper=true, includeFieldNames=true, doNotUseGetters = true)//ToString pode ser em formato JSON
public class UserJwt implements Serializable {

    private static final long serialVersionUID = -7132305669265631027L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_identificator")
    private Integer identificator;
    @NotBlank
    private String login;
    @NotBlank
    private String password;
    @NotBlank
    private String email;
    @NotBlank
    private String role;

    //TODO O equals, o hash, os getters e setters são do lombok
    @Override
    public String toString() {
        return ToStringUtils.toString(this);
    }

    public static UserDto userJwtToUserDto (UserJwt userJwt) {
        return UserDto.builder()
                      .login(userJwt.getLogin())
                      .password(userJwt.getPassword())
                      .email(userJwt.getEmail())
                      .role(RoleTypeEnum.valueOf(userJwt.getRole()))
                      .build();
    }

}
