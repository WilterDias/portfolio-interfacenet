package br.com.alterdata.bimer.prova.modelo;

public enum ContactType {
    EMAIL(Constants.EMAIL_VALUE),
    JOB(Constants.JOB_VALUE),
    PHONE(Constants.PHONE_VALUE),
    MOBILE(Constants.MOBILE_VALUE);

    private static final ContactType[] values = ContactType.values();

    private static class Constants {
        public static final String EMAIL_VALUE = "EMAIL";
        public static final String JOB_VALUE = "JOB";
        public static final String PHONE_VALUE = "PHONE";
        public static final String MOBILE_VALUE = "MOBILE";
    }

    private final String label;

    private ContactType(String label) {
        this.label = label;
    }

    public static ContactType getById(int id) {
        return values[id];
    }

    public String getLabel(){
        return this.label;
    }
}
