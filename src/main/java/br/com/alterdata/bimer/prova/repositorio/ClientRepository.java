package br.com.alterdata.bimer.prova.repositorio;

import br.com.alterdata.bimer.prova.modelo.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends JpaRepository<Client, Integer>, JpaSpecificationExecutor<Client> {
    //TODO Foi colocado o JPA pois ele estende crudRepository e PagingAndSorting repository, ele é melhor.
}
