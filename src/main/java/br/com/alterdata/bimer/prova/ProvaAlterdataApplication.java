package br.com.alterdata.bimer.prova;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages="br.com.alterdata.bimer")
//@EntityScan(basePackages="br.com.alterdata.bimer.prova.modelo")
//@EnableJpaRepositories(basePackages="br.com.alterdata.bimer.prova.repositorio")
public class ProvaAlterdataApplication {
	//TODO O ideal seria cada módulo ter os seus serviços, controlador, dtos, exceções, repositórios e modelos em uma pasta
	//TODO O ideal seria fazer os serviços, repositórios e dtos com o uso de interfaces funcionais com o @FunctionalInterface, pois evita o crescimento excessivo da classe.
	public static void main(String[] args) {
		SpringApplication.run(ProvaAlterdataApplication.class, args);
	}

}
