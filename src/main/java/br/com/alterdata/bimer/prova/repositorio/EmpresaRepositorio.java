package br.com.alterdata.bimer.prova.repositorio;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.alterdata.bimer.prova.modelo.Empresa;

@Repository
public interface EmpresaRepositorio extends CrudRepository<Empresa, Integer> {

}
