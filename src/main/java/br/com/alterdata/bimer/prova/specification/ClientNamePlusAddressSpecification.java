package br.com.alterdata.bimer.prova.specification;

import br.com.alterdata.bimer.prova.modelo.Client;
import br.com.alterdata.bimer.prova.modelo.Client_;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Expression;

public interface ClientNamePlusAddressSpecification {
    public static Specification<Client> name(String name) {
        return (root, criteriaQuery, criteriaBuilder) -> {
            Expression<String> r = criteriaBuilder.upper(root.get(Client_.name));
            return criteriaBuilder.like(r, name.toUpperCase()); //ilike
        };
    }

    public static Specification<Client> address(String address) {
        return (root, criteriaQuery, criteriaBuilder) -> {
            Expression<String> r = criteriaBuilder.upper(root.get(Client_.address));
            return criteriaBuilder.like(r, address.toUpperCase()); //ilike
        };
    }
}
