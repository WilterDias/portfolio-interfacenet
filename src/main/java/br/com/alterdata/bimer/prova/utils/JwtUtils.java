package br.com.alterdata.bimer.prova.utils;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.alterdata.bimer.prova.modelo.RoleTypeEnum;
import io.jsonwebtoken.JwtException;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;

@ConfigurationProperties(prefix = "jwt")
@Configuration("jwtProperties")
@Service
public class JwtUtils {

    private String secret;
    
    private Integer expirationDateInMs;
    
    private Integer restoreExpirationDateInMs;

    private static final SimpleGrantedAuthority AUTHORITY_USER  = new SimpleGrantedAuthority(RoleTypeEnum.ROLE_USER.getLabel());
    private static final SimpleGrantedAuthority AUTHORITY_ADMIN = new SimpleGrantedAuthority(RoleTypeEnum.ROLE_ADMIN.getLabel());

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public void setExpirationDateInMs(Integer expirationDateInMs) {
        this.expirationDateInMs = expirationDateInMs;
    }

    public void setRestoreExpirationDateInMs(Integer restoreExpirationDateInMs) {
        this.restoreExpirationDateInMs = restoreExpirationDateInMs;
    }

    public String generateToken(UserDetails userDetails) {
        Map<String, Object> claims = new HashMap<>();

        if (userDetails.getAuthorities()
                       .contains(JwtUtils.AUTHORITY_USER)) {
            claims.put("isUser", true);
        }
        if (userDetails.getAuthorities()
                       .contains(JwtUtils.AUTHORITY_ADMIN)) {
            claims.put("isAdmin", true);
        }

        return generateToken(userDetails.getUsername(), claims);
    }

    private String generateToken(String subject, Map<String, Object> claims) {
        return Jwts.builder()
                   .setClaims(claims)
                   .setIssuedAt(new Date(System.currentTimeMillis()))
                   .setExpiration(new Date(System.currentTimeMillis() + expirationDateInMs))
                   .setSubject(subject)
                   .signWith(SignatureAlgorithm.HS512, secret).compact();

    }

    public String generateRestoreToken(String subject, Map<String, Object> claims) {
        return Jwts.builder()
                   .signWith(SignatureAlgorithm.HS512, secret)
                   .setClaims(claims)
                   .setIssuedAt(new Date(System.currentTimeMillis()))
                   .setExpiration(new Date(System.currentTimeMillis() + restoreExpirationDateInMs))
                   .setSubject(subject)
                   .compact();
    }

    public Boolean validateToken(String authToken) {
        try {
            return Jwts.parser()
                       .setSigningKey(secret)
                       .parseClaimsJws(authToken) != null;
        } catch (ExpiredJwtException ex) {
            throw ex;
        } catch (JwtException ex){
            throw new BadCredentialsException("INVALID_CREDENTIALS", ex);
        }
    }

    public String getUsernameFromToken(String token) {
        return Jwts.parser()
                   .setSigningKey(secret)
                   .parseClaimsJws(token)
                   .getBody()
                   .getSubject();
    }

    public List<SimpleGrantedAuthority> getRolesFromToken(String token) {
        Claims claims = Jwts.parser()
                            .setSigningKey(secret)
                            .parseClaimsJws(token)
                            .getBody();

        Boolean isAdmin = claims.get("isAdmin", Boolean.class);
        Boolean isUser  = claims.get("isUser", Boolean.class);

        return (isUser != null && isUser)
            ? Collections.singletonList(JwtUtils.AUTHORITY_USER)
            : (isAdmin != null && isAdmin)
                ? Collections.singletonList(JwtUtils.AUTHORITY_ADMIN)
                : null;
    }

}
