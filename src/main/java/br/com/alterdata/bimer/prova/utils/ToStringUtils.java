package br.com.alterdata.bimer.prova.utils;

import java.lang.reflect.Field;

public final class ToStringUtils {
    //TODO O equals, o hash, os getters e setters são do lombok
    public static String toString(Object obj) {
        Field[] fields = obj.getClass()
                            .getDeclaredFields();
        StringBuilder res = new StringBuilder();
        res.append(obj.getClass()
                      .getSimpleName())
                      .append(" [");
        Boolean isComma = Boolean.FALSE;
        for (Field field : fields) {
            field.setAccessible(true);
            try {
                res.append((field.get(obj)) != null
                        ? getComma(isComma) + field.getName() + "=" + (field.get(obj).toString())
                        : "");
            } catch (Exception ignored) {

            }
            isComma = Boolean.TRUE;
        }
        return res.append("]")
                  .toString();
    }

    private static String getComma(Boolean isWanting){
        return Boolean.TRUE.equals(isWanting)?", ":"";
    }
}
