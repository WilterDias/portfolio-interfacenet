package br.com.alterdata.bimer.prova.dtos.user;

import br.com.alterdata.bimer.prova.modelo.RoleTypeEnum;
import br.com.alterdata.bimer.prova.modelo.UserJwt;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {
    @NotBlank(message = "{error.userLoginBlank}")
    private String login;
    @NotBlank(message = "{error.userPasswordBlank}")
    private String password;
    @Email(message = "{error.userEmailFormat}")
    private String email;
    @NotNull(message = "{error.userRoleTypeBlank}")
    private RoleTypeEnum role;

    public static UserJwt UserDtoToUserJwt(Integer userIdentificator, UserDto userDto) {
        return UserJwt.builder()
                      .identificator(userIdentificator)
                      .login(userDto.getLogin())
                      .password(userDto.getPassword())
                      .email(userDto.getEmail())
                      .role(userDto.getRole()
                                   .getLabel())
                      .build();
    }
}
