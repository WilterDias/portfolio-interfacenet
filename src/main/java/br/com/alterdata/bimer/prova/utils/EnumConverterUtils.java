package br.com.alterdata.bimer.prova.utils;

import br.com.alterdata.bimer.prova.controller.LogicOperatorEnum;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class EnumConverterUtils implements Converter<String, LogicOperatorEnum> {

    @Override
    public LogicOperatorEnum convert(String value) {
        return LogicOperatorEnum.valueOf(value.toUpperCase());
    }
}
