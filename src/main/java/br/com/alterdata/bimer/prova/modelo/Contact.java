package br.com.alterdata.bimer.prova.modelo;

import br.com.alterdata.bimer.prova.dtos.client.ContactDto;
import br.com.alterdata.bimer.prova.utils.ToStringUtils;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.validation.constraints.NotBlank;

import java.io.Serializable;

@Entity
@Table(name = "contact")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
//@ToString(callSuper=true, includeFieldNames=true, doNotUseGetters = true)//ToString pode ser em formato JSON
public class Contact implements Serializable{
    private static final long serialVersionUID = -4307647976187192388L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "contact_identificator")
    private Integer identificator;
    //@Enumerated(EnumType.STRING)
    //@Column(name = "contact_type")
    @NotBlank
    private String contactType;
    @NotBlank
    private String valueContact;
    private String description;

    //TODO O equals, o hash, os getters e setters são do lombok
    @Override
    public String toString() {
        return ToStringUtils.toString(this);
    }

    public static ContactDto contactToContactDto(Contact contact) {
        return ContactDto.builder()
                         .identificator(contact.getIdentificator())
                         .contactType(ContactType.valueOf(contact.getContactType()))
                         .valueContact(contact.getValueContact())
                         .description(contact.getDescription())
                         .build();

    }
}
