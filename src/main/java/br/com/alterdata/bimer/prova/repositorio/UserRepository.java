package br.com.alterdata.bimer.prova.repositorio;

import br.com.alterdata.bimer.prova.modelo.UserJwt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserJwt, Integer> {
    //TODO Foi colocado o JPA pois ele estende crudRepository e PagingAndSorting repository, ele é melhor.
    Optional<UserJwt> findByLogin(String login);

    void deleteByLogin(String login);
}
