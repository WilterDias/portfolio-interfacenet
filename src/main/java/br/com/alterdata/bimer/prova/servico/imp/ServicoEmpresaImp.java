package br.com.alterdata.bimer.prova.servico.imp;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.alterdata.bimer.prova.modelo.Empresa;
import br.com.alterdata.bimer.prova.repositorio.EmpresaRepositorio;
import br.com.alterdata.bimer.prova.servico.ServicoEmpresa;

@Service
public class ServicoEmpresaImp implements ServicoEmpresa{

	private EmpresaRepositorio empresaRespositorio;
	
	@Autowired
	public ServicoEmpresaImp(EmpresaRepositorio empresaRespositorio) {
		this.empresaRespositorio = empresaRespositorio;
	}

	@Override
	public Optional<Empresa> obterPorIdentificador(Integer identificador) {
		return empresaRespositorio.findById(identificador);
	}

	@Override
	public List<Empresa> obterTodos() {
		return (List<Empresa>) empresaRespositorio.findAll();
	}

	@Override
	public Empresa gravar(Empresa empresa) {
		return empresaRespositorio.save(empresa);
	}

	@Override
	public void remover(Integer identificador) {
		empresaRespositorio.deleteById(identificador);
	}

}
