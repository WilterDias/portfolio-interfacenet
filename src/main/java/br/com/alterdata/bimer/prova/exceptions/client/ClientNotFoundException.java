package br.com.alterdata.bimer.prova.exceptions.client;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "exception.clientNotFound")
public class ClientNotFoundException extends Exception {}
