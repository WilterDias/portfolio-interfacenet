package br.com.alterdata.bimer.prova.dtos.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AuthenticationRequestDto {
    @NotBlank(message = "{error.userLoginBlank}")
    private String login;
    @NotBlank(message = "{error.userPasswordBlank}")
    private String password;

}
