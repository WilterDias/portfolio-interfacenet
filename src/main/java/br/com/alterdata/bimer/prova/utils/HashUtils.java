package br.com.alterdata.bimer.prova.utils;

import java.lang.reflect.Field;

public final class HashUtils {
    //TODO O equals, o hash, os getters e setters são do lombok
    public static Integer hash(Object thisobj) {
        final int prime = 31;
        int result = 1;
        Field[] fields = thisobj.getClass().getDeclaredFields();
        for (Field field : fields) {
            try {
                result = prime * result + ((field.get(thisobj) == null) ? 0 : field.get(thisobj).hashCode());
            } catch (Exception ignored) {
            }
        }
        return result;
    }

    private static String getComma(Boolean isWanting){
        return Boolean.TRUE.equals(isWanting)?", ":"";
    }
}
