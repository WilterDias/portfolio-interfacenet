package br.com.alterdata.bimer.prova.modelo;

import br.com.alterdata.bimer.prova.dtos.client.ClientDto;
import br.com.alterdata.bimer.prova.dtos.client.ContactDto;
import br.com.alterdata.bimer.prova.utils.HashUtils;
import br.com.alterdata.bimer.prova.utils.ToStringUtils;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import org.springframework.beans.factory.annotation.Value;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "client")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
//@ToString(callSuper=true, includeFieldNames=true, doNotUseGetters = true)//ToString pode ser em formato JSON
public class Client implements Serializable{

    private static final long serialVersionUID = 3160956559315039929L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "client_identificator")
    private Integer identificator;
    @NotBlank
    private String name;
    @NotNull
    @NotEmpty
    @Builder.Default
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Contact> contactList = new ArrayList<>();
    @NotBlank
    private String address;
    @NotNull
    @Column(name = "bith_date")
    private LocalDate birthDate;
    @NotNull
    @Value("true")
    private Boolean status;

    //TODO O equals, o hash, os getters e setters são do lombok
    @Override
    public String toString() {
        return ToStringUtils.toString(this);
    }

    @Override
    public int hashCode() {
        return HashUtils.hash(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }
        try {
            Client other = (Client) obj;
            Field[] fields = this.getClass().getDeclaredFields();
            for (Field field : fields) {
                if ((field.get(this) != null)
                        && (field.get(other) != null)
                        && (field.get(this) instanceof ArrayList)) {
                    //TODO Acredito haver uma forma melhor para comparar os tipos, usando Type ou TypeToken
                    if (!(field.get(other) instanceof ArrayList)
                            //|| (!((ArrayList<?>) field.get(this)).isEmpty()
                            //&&  !((ArrayList<?>) field.get(other)).isEmpty()
                            //&&  (((ArrayList<?>) field.get(this)).get(0) instanceof Client)
                            //&&  (((ArrayList<?>) field.get(other)).get(0) instanceof Client))
                            //||  !(((ArrayList<?>) field.get(this))
                            //                           .containsAll((ArrayList<?>) field.get(other))
                            //      && ((ArrayList<?>) field.get(other))
                            //                                .containsAll((ArrayList<?>) field.get(this)))) {
                            || !(new HashSet<>((ArrayList<?>) field.get(this)))
                            .equals(new HashSet<>((ArrayList<?>) field.get(other)))) {
                        return false;
                    }
                } else if (field.get(this) == null) {
                    if (field.get(other) != null)
                        return false;
                } else if (!field.get(this).equals(field.get(other)))
                    return false;

            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public static ClientDto clientToClientDto(Client client){
        return ClientDto.builder()
                        .identificator(client.getIdentificator())
                        .name(client.getName())
                        .address(client.getAddress())
                        .contactList(contactListDtoToContactList(client.getContactList()))
                        .birthDate(client.getBirthDate())
                        .status(client.getStatus())
                        .build();
    }

    private static List<ContactDto> contactListDtoToContactList(List<Contact> contactList){
        return contactList.stream()
                          .map(Contact::contactToContactDto)
                          .collect(Collectors.toList());
    }
}
