package br.com.alterdata.bimer.prova.controller;

import br.com.alterdata.bimer.prova.dtos.user.UserDto;
import br.com.alterdata.bimer.prova.exceptions.user.UserLoginExistException;
import br.com.alterdata.bimer.prova.exceptions.user.UserLoginNotFoundException;
import br.com.alterdata.bimer.prova.modelo.RoleTypeEnum;
import br.com.alterdata.bimer.prova.modelo.UserJwt;
import br.com.alterdata.bimer.prova.servico.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@RequestMapping(value="/api/users")
public class UserController {

    private final UserService userService;

    @Secured({RoleTypeEnum.Constants.ROLE_ADMIN})
    @GetMapping("/{userLogin}")
    public Optional<UserDto> getByLogin(@PathVariable String userLogin) throws UserLoginNotFoundException {
        return this.userService.getByLogin(userLogin);//TODO A exceção já envia o código HTTP do problema
    }

    @Secured({RoleTypeEnum.Constants.ROLE_ADMIN})
    @GetMapping
    public List<UserDto> listAll() {
        return this.userService.listAll();
    }

    @Secured({RoleTypeEnum.Constants.ROLE_ADMIN})
    @PutMapping(value = "/")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Optional<UserDto> update(
            @Valid @RequestBody UserDto userDto
    ) throws UserLoginNotFoundException {
        return this.userService.update(userDto);//TODO A exceção já envia o código HTTP do problema
    }

    @Secured({RoleTypeEnum.Constants.ROLE_ADMIN})
    @DeleteMapping(value = "/{userLogin}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable String userLogin) throws UserLoginNotFoundException {
        this.userService.remove(userLogin); // no content
    }

    //Não há permissão para criar um usuário.
    @PostMapping(value = "/")
    @ResponseStatus(HttpStatus.CREATED)
    public UserJwt save(@RequestBody UserDto user) throws UserLoginExistException {
        return userService.save(user);
    }

}
