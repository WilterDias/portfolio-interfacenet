package br.com.alterdata.bimer.prova.servico;

import br.com.alterdata.bimer.prova.dtos.client.ClientDto;
import br.com.alterdata.bimer.prova.dtos.client.RecordClientDto;
import br.com.alterdata.bimer.prova.exceptions.client.ClientNotFoundException;
import br.com.alterdata.bimer.prova.exceptions.client.DontHaveTwoDistinctContactTypeException;

import java.util.List;
import java.util.Optional;

public interface ClientService {

    Optional<ClientDto> getByIdentificator(Integer clientIdentificator) throws ClientNotFoundException;

    List<ClientDto> listAll();

    Optional<ClientDto> save(RecordClientDto recordClientDto) throws DontHaveTwoDistinctContactTypeException;

    Optional<ClientDto> update(Integer clientIdentificator, RecordClientDto recordClientDto)
            throws ClientNotFoundException, DontHaveTwoDistinctContactTypeException;

    List<ClientDto> findByNamePlusAddress(String logicOperator, String name, String address);

    void remove(Integer identificador) throws ClientNotFoundException;

//    void removeContactClient(Integer clientIdentificator, Integer contactClientIdentificator)
//            throws ContactClientNotFoundException, DontHaveTwoDistinctContactTypeException, ClientNotFoundException;
}
