package br.com.alterdata.bimer.prova.controller;

import br.com.alterdata.bimer.prova.dtos.client.ClientDto;
import br.com.alterdata.bimer.prova.dtos.client.RecordClientDto;
import br.com.alterdata.bimer.prova.exceptions.client.ClientNotFoundException;
import br.com.alterdata.bimer.prova.exceptions.client.DontHaveTwoDistinctContactTypeException;
import br.com.alterdata.bimer.prova.modelo.RoleTypeEnum;
import br.com.alterdata.bimer.prova.servico.ClientService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@RequestMapping(value="/api/clients")
public class ClientController {

    private final ClientService clientService;

    @Secured({RoleTypeEnum.Constants.ROLE_USER, RoleTypeEnum.Constants.ROLE_ADMIN})
    @GetMapping("/{clientIdentificator}")
    public Optional<ClientDto> getByIdentificator(@PathVariable Integer clientIdentificator)
            throws ClientNotFoundException {
        return this.clientService.getByIdentificator(clientIdentificator);//TODO A exceção já envia o código HTTP do problema
    }

    @Secured({RoleTypeEnum.Constants.ROLE_USER, RoleTypeEnum.Constants.ROLE_ADMIN})
    @GetMapping
    public List<ClientDto> listAll() {
        return this.clientService.listAll();
    }

    @Secured({RoleTypeEnum.Constants.ROLE_USER, RoleTypeEnum.Constants.ROLE_ADMIN})
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Optional<ClientDto> save(@Valid @RequestBody RecordClientDto recordClientDto) 
            throws DontHaveTwoDistinctContactTypeException {
        return this.clientService.save(recordClientDto);
    }

    @Secured({RoleTypeEnum.Constants.ROLE_USER, RoleTypeEnum.Constants.ROLE_ADMIN})
    @PutMapping(value = "/{clientIdentificator}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Optional<ClientDto> update(
            @PathVariable Integer clientIdentificator,
            @Valid @RequestBody RecordClientDto recordClientDto
    ) throws ClientNotFoundException, DontHaveTwoDistinctContactTypeException {
        return this.clientService.update(clientIdentificator, recordClientDto);//TODO A exceção já envia o código HTTP do problema
    }

    @Secured({RoleTypeEnum.Constants.ROLE_USER, RoleTypeEnum.Constants.ROLE_ADMIN})
    @DeleteMapping(value = "/{clientIdentificator}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable Integer clientIdentificator) throws ClientNotFoundException {
        this.clientService.remove(clientIdentificator); // no content
    }

    @Secured({RoleTypeEnum.Constants.ROLE_ADMIN})
    @GetMapping(value = "/search")
    public List<ClientDto> searchNamePlusAddress(
            @RequestParam(value = "logic") LogicOperatorEnum logicOperatorEnum,//AND ou OR
            @RequestParam(value = "name") String name,
            @RequestParam(value = "address") String address) {
        return this.clientService.findByNamePlusAddress(logicOperatorEnum.getLabel(), name, address);
    }

//    @DeleteMapping(value = "/{clientIdentificator}/{contactClientIdentificator}")
//    @ResponseStatus(HttpStatus.NO_CONTENT)
//    public void removeContactClient(@PathVariable Integer clientIdentificator,
//                                    @PathVariable Integer contactClientIdentificator)
//            throws ContactClientNotFoundException, DontHaveTwoDistinctContactTypeException, ClientNotFoundException {
//        this.clientService.removeContactClient(clientIdentificator, contactClientIdentificator); // no content
//    }
}
