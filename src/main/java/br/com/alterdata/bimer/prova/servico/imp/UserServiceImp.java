package br.com.alterdata.bimer.prova.servico.imp;

import br.com.alterdata.bimer.prova.dtos.user.UserDto;
import br.com.alterdata.bimer.prova.exceptions.user.UserLoginExistException;
import br.com.alterdata.bimer.prova.exceptions.user.UserLoginNotFoundException;
import br.com.alterdata.bimer.prova.modelo.UserJwt;
import br.com.alterdata.bimer.prova.repositorio.UserRepository;
import br.com.alterdata.bimer.prova.servico.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserServiceImp implements UserService {

    private final UserRepository userRepository;

    private final PasswordEncoder cryptographyEncoder;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException{
        Optional<UserJwt> userJwt = userRepository.findByLogin(login);
        if (userJwt.isEmpty()) {
            throw new UsernameNotFoundException("");
        }
        return new User(userJwt.get()
                               .getLogin(),
                        userJwt.get()
                               .getPassword(),
                        Collections.singletonList(new SimpleGrantedAuthority(userJwt.get()
                                                                                    .getRole())));
    }

    @Override
    public UserJwt save(UserDto userDto) throws UserLoginExistException {
        if (userRepository.findByLogin(userDto.getLogin()).isPresent()) {
            throw new UserLoginExistException();
        }
        userDto.setPassword(cryptographyEncoder.encode(userDto.getPassword()));
        return userRepository.save(UserDto.UserDtoToUserJwt(null, userDto));
    }

    @Override
    public Optional<UserDto> getByLogin(String userLogin) throws UserLoginNotFoundException {
        Optional<UserJwt> userJwt = userRepository.findByLogin(userLogin);
        if (userJwt.isEmpty()) {
            throw new UserLoginNotFoundException();
        }
        return Optional.of(UserJwt.userJwtToUserDto(userJwt.get()));
    }

    @Override
    public List<UserDto> listAll() {
        List<UserJwt> listClient = userRepository.findAll();
        return listClient.stream()
                         .map(UserJwt::userJwtToUserDto)
                         .collect(Collectors.toList());
    }

    @Override
    public Optional<UserDto> update(UserDto userDto) throws UserLoginNotFoundException {
        UserJwt existing = userRepository.findByLogin(userDto.getLogin())
                                         .orElseThrow(UserLoginNotFoundException::new);
        userDto.setPassword(cryptographyEncoder.encode(userDto.getPassword()));
        UserJwt userJwt = userRepository.save(UserDto.UserDtoToUserJwt(existing.getIdentificator(), userDto));
        return Optional.of(UserJwt.userJwtToUserDto(userJwt));
    }

    @Override
    public void remove(String userLogin) throws UserLoginNotFoundException {
        UserJwt existing = userRepository.findByLogin(userLogin)
                                         .orElseThrow(UserLoginNotFoundException::new);
        userRepository.deleteById(existing.getIdentificator());
    }

}
