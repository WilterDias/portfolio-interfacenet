package br.com.alterdata.bimer.prova.modelo;

public enum RoleTypeEnum {
    ROLE_USER(Constants.ROLE_USER),
    ROLE_ADMIN(Constants.ROLE_ADMIN);

    private static final RoleTypeEnum[] values = RoleTypeEnum.values();

    public static class Constants {
        public static final String ROLE_USER = "ROLE_USER";
        public static final String ROLE_ADMIN = "ROLE_ADMIN";
    }

    private final String label;

    private RoleTypeEnum(String label) {
        this.label = label;
    }

    public static RoleTypeEnum getById(int id) {
        return values[id];
    }

    public String getLabel(){
        return this.label;
    }
}
