package br.com.alterdata.bimer.prova.exceptions.user;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "exception.userLoginNotFound")
public class UserLoginNotFoundException extends Exception {}
