package br.com.alterdata.bimer.prova.servico;

import br.com.alterdata.bimer.prova.dtos.user.UserDto;

import br.com.alterdata.bimer.prova.exceptions.user.UserLoginExistException;
import br.com.alterdata.bimer.prova.exceptions.user.UserLoginNotFoundException;
import br.com.alterdata.bimer.prova.modelo.UserJwt;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;
import java.util.Optional;

public interface UserService extends UserDetailsService {

    UserJwt save(UserDto userDto) throws UserLoginExistException;

    Optional<UserDto> getByLogin(String userLogin) throws UserLoginNotFoundException;

    List<UserDto> listAll();

    Optional<UserDto> update(UserDto userDto) throws UserLoginNotFoundException;

    void remove(String userLogin) throws UserLoginNotFoundException;
}
