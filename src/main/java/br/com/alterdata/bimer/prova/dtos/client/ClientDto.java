package br.com.alterdata.bimer.prova.dtos.client;

import br.com.alterdata.bimer.prova.modelo.Client;
import br.com.alterdata.bimer.prova.modelo.Contact;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClientDto {
    @NotNull(message = "{error.clientIdentificatorNull}")
    private Integer identificator;
    @NotBlank(message = "{error.clientNameBlank}")
    private String name;
    @NotBlank(message = "{error.clientAdressBlank}")
    private String address;
    @NotNull(message = "{error.clienteContactListNull}")
    @NotEmpty(message = "{error.clienteContactListEmpty}")
    private List<@Valid ContactDto> contactList = new ArrayList<>();
    @NotNull(message = "{error.clienteBirthDateNull}")
    @JsonFormat(pattern="dd-MM-yyyy")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate birthDate;
    @NotNull(message = "{error.clienteStatusNull}")
    @Value("true")
    private Boolean status;

    public static Client clientDtoToClient(ClientDto clientDto){
        return Client.builder()
                     .identificator(clientDto.getIdentificator())
                     .name(clientDto.getName())
                     .address(clientDto.getAddress())
                     .contactList(contactListDtoToContactList(clientDto.getContactList()))
                     .birthDate(clientDto.getBirthDate())
                     .status(clientDto.getStatus())
                     .build();
    }

    private static List<Contact> contactListDtoToContactList(List<ContactDto> contactListDto){
        return contactListDto.stream()
                             .map(contactDto->ContactDto.contactDtoToContact(Boolean.TRUE, contactDto))
                             .collect(Collectors.toList());
    }
}
