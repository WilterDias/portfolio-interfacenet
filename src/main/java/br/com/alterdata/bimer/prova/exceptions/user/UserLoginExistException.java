package br.com.alterdata.bimer.prova.exceptions.user;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "exception.userLoginExistException")
public class UserLoginExistException extends Exception {}
