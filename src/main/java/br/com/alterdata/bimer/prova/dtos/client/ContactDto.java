package br.com.alterdata.bimer.prova.dtos.client;

import br.com.alterdata.bimer.prova.modelo.Contact;
import br.com.alterdata.bimer.prova.modelo.ContactType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ContactDto {
    private Integer identificator;
    @NotNull(message = "{error.clienteContactTypeInListBlank}")
    private ContactType contactType;
    @NotBlank(message = "{error.clienteContactValueInListBlank}")
    private String valueContact;
    private String description;

    public static Contact contactDtoToContact(Boolean hasIdentificator, ContactDto contactDto) {
        return Contact.builder()
                      .identificator(hasIdentificator
                                             ?contactDto.getIdentificator()
                                             :null)
                      .contactType(contactDto.getContactType().getLabel())
                      .valueContact(contactDto.getValueContact())
                      .description(contactDto.getDescription())
                      .build();

    }
}
