package br.com.alterdata.bimer.prova.exceptions.user;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.UNAUTHORIZED, reason = "exception.invalidTokenCredentials")
public class InvalidTokenCredentialsException extends Exception {}
