package br.com.alterdata.bimer.prova.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.alterdata.bimer.prova.modelo.Empresa;
import br.com.alterdata.bimer.prova.servico.ServicoEmpresa;

@RestController
@RequestMapping("/api/empresas")
public class EmpresaController {

	private ServicoEmpresa servicoEmpresa;

	@Autowired
	public EmpresaController(ServicoEmpresa servicoEmpresa) {
			this.servicoEmpresa = servicoEmpresa;
	}
	
	@GetMapping
	public List<Empresa> obterTodos(){
		return servicoEmpresa.obterTodos();
	}
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<Empresa> obterPorIdentificador(@PathVariable("id") Integer identificador){
		return servicoEmpresa.obterPorIdentificador(identificador)
				 .map(record -> ResponseEntity.ok().body(record))
		         .orElse(ResponseEntity.notFound().build());
	}
	
	@PostMapping()
	public ResponseEntity<Empresa> gravar(@RequestBody Empresa empresa) {
		var persistido  = servicoEmpresa.gravar(empresa);
		return ResponseEntity.ok(persistido);
	}
	
	@PutMapping
	public ResponseEntity<Empresa> atualizar(Integer identificador, @RequestBody Empresa empresa)
	{
		return servicoEmpresa.obterPorIdentificador(identificador)           
			.map(emp -> {
				emp.setRazaoSocial(empresa.getRazaoSocial());
				emp.setCnpj(empresa.getCnpj());
				var atualizado = emp;
	            return ResponseEntity.ok().body(atualizado);
	        }).orElse(ResponseEntity.notFound().build());
	}
	
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<?> delete(@PathVariable("id") Integer identificador) {
	   return servicoEmpresa.obterPorIdentificador(identificador)
	           .map(record -> {
	        	   servicoEmpresa.remover(identificador);
	               return ResponseEntity.noContent().build();
	           }).orElse(ResponseEntity.notFound().build());
	}
}
