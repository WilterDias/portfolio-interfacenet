package br.com.alterdata.bimer.prova.controller;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.Arrays;

public enum LogicOperatorEnum {
    AND(Constants.AND),
    OR(Constants.OR);

    private static final LogicOperatorEnum[] values = LogicOperatorEnum.values();

    private static class Constants {
        public static final String AND = "AND";
        public static final String OR = "OR";
    }

    private final String label;

    private LogicOperatorEnum(String label) {
        this.label = label;
    }

    public static LogicOperatorEnum getById(int id) {
        return values[id];
    }

    public String getLabel(){
        return this.label;
    }

}