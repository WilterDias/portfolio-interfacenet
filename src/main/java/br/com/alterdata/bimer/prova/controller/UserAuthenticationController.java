package br.com.alterdata.bimer.prova.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import br.com.alterdata.bimer.prova.dtos.user.AuthenticationRequestDto;
import br.com.alterdata.bimer.prova.dtos.user.AuthenticationResponseDto;
import br.com.alterdata.bimer.prova.exceptions.user.InvalidTokenCredentialsException;
import br.com.alterdata.bimer.prova.exceptions.user.UserDisabledException;
import br.com.alterdata.bimer.prova.exceptions.user.UserLoginNotFoundException;
import br.com.alterdata.bimer.prova.servico.UserService;
import br.com.alterdata.bimer.prova.utils.JwtUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.jsonwebtoken.impl.DefaultClaims;

@RestController
@RequiredArgsConstructor
@RequestMapping(value="/api/users")
public class UserAuthenticationController {
    
    private final AuthenticationManager authenticationManager;
    
    private final UserService userService;

    private final JwtUtils jwtUtils;

    @PostMapping(value = "/authenticate")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public AuthenticationResponseDto generateToken(@RequestBody AuthenticationRequestDto authenticationRequestDto)
            throws UserDisabledException, InvalidTokenCredentialsException, UserLoginNotFoundException {
        try {
            this.authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            authenticationRequestDto.getLogin(),
                            authenticationRequestDto.getPassword()));

            UserDetails userDetails = this.userService.loadUserByUsername(authenticationRequestDto.getLogin());
            return new AuthenticationResponseDto(this.jwtUtils.generateToken(userDetails));
        } catch (DisabledException e) {
            throw new UserDisabledException();
        } catch (BadCredentialsException e) {
            throw new InvalidTokenCredentialsException();
        } catch (UsernameNotFoundException e) {
            throw new UserLoginNotFoundException();
        }

    }

    @GetMapping("/restoreToken")
    public AuthenticationResponseDto restoretoken(HttpServletRequest request) {
        Map<String, Object> claimMap = new HashMap<String, Object>();
        ((DefaultClaims) request.getAttribute("claims"))
                .forEach(claimMap::put);
        String token = this.jwtUtils.generateRestoreToken(claimMap.get("sub")
                .toString(), claimMap);
        return new AuthenticationResponseDto(token);
    }

}

