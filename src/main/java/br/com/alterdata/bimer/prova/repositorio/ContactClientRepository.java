package br.com.alterdata.bimer.prova.repositorio;

import br.com.alterdata.bimer.prova.modelo.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContactClientRepository extends JpaRepository<Contact, Integer> {
    //TODO Foi colocado o JPA pois ele estende crudRepository e PagingAndSorting repository, ele é melhor.
}
