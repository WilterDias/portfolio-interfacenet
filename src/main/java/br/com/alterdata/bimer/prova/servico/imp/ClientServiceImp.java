package br.com.alterdata.bimer.prova.servico.imp;


import br.com.alterdata.bimer.prova.dtos.client.ClientDto;
import br.com.alterdata.bimer.prova.dtos.client.ContactDto;
import br.com.alterdata.bimer.prova.dtos.client.RecordClientDto;
import br.com.alterdata.bimer.prova.exceptions.client.ClientNotFoundException;
import br.com.alterdata.bimer.prova.exceptions.client.DontHaveTwoDistinctContactTypeException;
import br.com.alterdata.bimer.prova.modelo.Client;
import br.com.alterdata.bimer.prova.repositorio.ClientRepository;
import br.com.alterdata.bimer.prova.repositorio.ContactClientRepository;
import br.com.alterdata.bimer.prova.servico.ClientService;
import br.com.alterdata.bimer.prova.specification.ClientNamePlusAddressSpecification;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.stream.Collectors;

import static org.springframework.data.jpa.domain.Specification.where;

@Service
@RequiredArgsConstructor
public class ClientServiceImp implements ClientService {

    @NonNull
    private final ClientRepository clientRepository;
    @NonNull
    private final ContactClientRepository contactClientRepository;

    @Override
    public Optional<ClientDto> getByIdentificator(Integer clientIdentificator) throws ClientNotFoundException {
        Client client = this.clientRepository.findById(clientIdentificator)
                                             .orElseThrow(ClientNotFoundException::new);
        return Optional.of(Client.clientToClientDto(client));
    }

    @Override
    public List<ClientDto> listAll() {
        List<Client> listClient = this.clientRepository.findAll();
        return listClient.stream()
                         .map(Client::clientToClientDto)
                         .collect(Collectors.toList());
    }

    @Override
    public Optional<ClientDto> save(RecordClientDto recordClientDto) throws DontHaveTwoDistinctContactTypeException {
        //TODO Nos tipos de contatos phone e e-mail pode ser feita uma verificação de formato.
        if(Boolean.FALSE.equals(verifyContactInClient(recordClientDto))) {
            throw new DontHaveTwoDistinctContactTypeException();
        }
        Client client = this.clientRepository.save(RecordClientDto.clientDtoToClient(null,
                                                                                     recordClientDto));
        return Optional.of(Client.clientToClientDto(client));
    }
    private Boolean verifyContactInClient(RecordClientDto recordClientDto) {
        List<ContactDto> contactList = recordClientDto.getContactList();
        if (contactList == null
                || contactList.isEmpty()
                || !haveTwoOrMoreContactDtoDifferent(contactList)
        ) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }
    private Boolean haveTwoOrMoreContactDtoDifferent(List<ContactDto> contactList) {
        return contactList.stream()
                          .map(ContactDto::getContactType)
                          .distinct()
                          .count() > 1; //Verifica se tem mais de 1 tipo de contrato distintos.
    }

    @Override
    public Optional<ClientDto> update(Integer clientIdentificator, RecordClientDto recordClientDto)
            throws ClientNotFoundException, DontHaveTwoDistinctContactTypeException {
        //TODO A lista de contatos do cliente é substituida no update.
        if(Boolean.FALSE.equals(verifyContactInClient(recordClientDto))) {
            throw new DontHaveTwoDistinctContactTypeException();
        }
        Client existing = clientRepository.findById(clientIdentificator)
                                          .orElseThrow(ClientNotFoundException::new);
        Client client = clientRepository.save(RecordClientDto.clientDtoToClient(clientIdentificator,
                                                                                recordClientDto));
        return Optional.of(Client.clientToClientDto(client));
    }

    @Override
    public void remove(Integer identificator) throws ClientNotFoundException {
        try {
            this.clientRepository.deleteById(identificator);
        } catch (EmptyResultDataAccessException e) {
            throw new ClientNotFoundException();
        }
    }

    @Override
    public List<ClientDto> findByNamePlusAddress(String logicOperator, String name, String address) {
        List<Client> listClient;
        Specification<Client> clientName = where(ClientNamePlusAddressSpecification.name(name));
        Specification<Client> clientAdress = ClientNamePlusAddressSpecification.address(address);
        Specification<Client> putOperator = null;
        if (logicOperator != null && !logicOperator.isBlank()) {
            putOperator = logicOperator.toLowerCase().equals("and")
                            ? clientName.and(clientAdress)
                            : logicOperator.toLowerCase().equals("or")
                                ? clientName.or(clientAdress)
                                : null;
        }
        listClient = clientRepository.findAll(putOperator);
        return listClient.stream()
                         .map(Client::clientToClientDto)
                         .collect(Collectors.toList());
    }

//    @Override
//    public Page<ClientDto> findAll(ClientSpecification<Client> clientSearchSpec, Pageable pageable) {
//       // this.clientRepository.findAll(clientSearchSpec, pageable);
//        return null;
//    }

//    @Transactional(rollbackFor = DontHaveTwoDistinctContactTypeException.class)
//    @Override
//    public void removeContactClient(Integer clientIdentificator,
//                                    Integer contactClientIdentificator) throws ContactClientNotFoundException,
//                                                                               DontHaveTwoDistinctContactTypeException,
//                                                                               ClientNotFoundException {
//        Client existingClient = this.clientRepository.findById(clientIdentificator)
//                                                .orElseThrow(ClientNotFoundException::new);
//        List<Contact> existingListContactFromClient = existingClient.getContactList();
//        if (existingListContactFromClient == null
//                || existingListContactFromClient.isEmpty()
//                || existingListContactFromClient.stream()
//                                                .anyMatch(contact -> contact.getIdentificator()
//                                                                            .equals(contactClientIdentificator))) {
//            throw new ContactClientNotFoundException();
//        }
//
//        try {
//            contactthis.clientRepository.deleteById(contactClientIdentificator);
//        } catch (EmptyResultDataAccessException e) {
//            throw new ContactClientNotFoundException();
//        }
//
//        if (!haveTwoOrMoreContactDifferent(existingListContactFromClient)) {
//            throw new DontHaveTwoDistinctContactTypeException();
//        }
//    }
}
