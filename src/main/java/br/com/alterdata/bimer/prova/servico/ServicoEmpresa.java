package br.com.alterdata.bimer.prova.servico;

import java.util.List;
import java.util.Optional;

import br.com.alterdata.bimer.prova.modelo.Empresa;

public interface ServicoEmpresa {

	Optional<Empresa> obterPorIdentificador(Integer identificador);
	List<Empresa> obterTodos();
	Empresa gravar(Empresa empresa);
	void remover(Integer identificador);
}
