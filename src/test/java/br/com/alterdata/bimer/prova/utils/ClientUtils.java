package br.com.alterdata.bimer.prova.utils;

import br.com.alterdata.bimer.prova.dtos.client.ClientDto;
import br.com.alterdata.bimer.prova.dtos.client.ContactDto;
import br.com.alterdata.bimer.prova.dtos.client.RecordClientDto;
import br.com.alterdata.bimer.prova.modelo.ContactType;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

public final class ClientUtils {
    private static final Integer   IDENTIFICATOR = 1;
    private static final String    NAME          = "Wilter";
    private static final String    ADDRESS       = "Rua tal";
    private static final LocalDate BIRTH_DATE    = LocalDate.of(2020, 1,1);
    private static final Boolean   STATUS        = Boolean.TRUE;

    public static RecordClientDto.RecordClientDtoBuilder createRecordClientDto() {
        return createRecordClientDtoWithEmptyContact().contactList(contactListDifferentContactType());
    }

    public static RecordClientDto.RecordClientDtoBuilder createRecordClientDtoWithoutDistinctContactType() {
        return createRecordClientDtoWithEmptyContact().contactList(contactListWithOneContactType());
    }

    public static ClientDto.ClientDtoBuilder createClientDto() {
        return createClientDtoWithEmptyContact().contactList(contactListDifferentContactType());
    }

    public static ClientDto.ClientDtoBuilder createClientDtoWithoutDistinctContactType() {
        return createClientDtoWithEmptyContact().contactList(contactListWithOneContactType());
    }

    public static ClientDto.ClientDtoBuilder createClientDtoWithEmptyContact() {
        return ClientDto.builder()
                        .identificator(IDENTIFICATOR)
                        .name(NAME)
                        .address(ADDRESS)
                        .birthDate(BIRTH_DATE)
                        .status(STATUS);
    }

    public static RecordClientDto.RecordClientDtoBuilder createRecordClientDtoWithEmptyContact() {
        return RecordClientDto.builder()
                              .name(NAME)
                              .address(ADDRESS)
                              .birthDate(BIRTH_DATE)
                              .status(STATUS);
    }

    private static List<ContactDto> contactListDifferentContactType(){
        return Arrays.asList(ContactDto.builder()
                                       .identificator(1)
                                       .contactType(ContactType.EMAIL)
                                       .valueContact("a@a.com")
                                       .description("Email da empresa")
                                       .build(),
                            ContactDto.builder()
                                      .identificator(1)
                                      .contactType(ContactType.PHONE)
                                      .valueContact("88 88 8 8888-8888")
                                      .description("Telefone da empresa")
                                      .build()
                             );
    }

    private static List<ContactDto> contactListWithOneContactType(){
        return Arrays.asList(ContactDto.builder()
                                       .identificator(1)
                                       .contactType(ContactType.EMAIL)
                                       .valueContact("a@a.com")
                                       .description("Email da empresa")
                                       .build(),
                ContactDto.builder()
                          .identificator(1)
                          .contactType(ContactType.EMAIL)
                          .valueContact("b@b.com")
                          .description("Email da empresa")
                          .build()
        );
    }
}
