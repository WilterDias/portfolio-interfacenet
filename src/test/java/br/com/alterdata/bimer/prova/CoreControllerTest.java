package br.com.alterdata.bimer.prova;

import br.com.alterdata.bimer.config.ProvaConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

@ActiveProfiles("test")
@Import(ProvaConfig.class)
public abstract class CoreControllerTest {

    @Autowired
    protected MockMvc mockMvc;
}
