package br.com.alterdata.bimer.prova.servico;

import br.com.alterdata.bimer.prova.controller.ClientController;
import br.com.alterdata.bimer.prova.dtos.client.ClientDto;
import br.com.alterdata.bimer.prova.dtos.client.RecordClientDto;
import br.com.alterdata.bimer.prova.exceptions.client.ClientNotFoundException;
import br.com.alterdata.bimer.prova.exceptions.client.DontHaveTwoDistinctContactTypeException;
import br.com.alterdata.bimer.prova.modelo.Client;
import br.com.alterdata.bimer.prova.repositorio.ClientRepository;
import br.com.alterdata.bimer.prova.repositorio.ContactClientRepository;
import br.com.alterdata.bimer.prova.servico.imp.ClientServiceImp;
import br.com.alterdata.bimer.prova.utils.ClientUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.ContextConfiguration;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

@ExtendWith(MockitoExtension.class)
@DisplayName("Valida o serviço do cliente")
@ContextConfiguration(classes = {ClientController.class,ClientService.class, ClientRepository.class})
public class ClientServiceTest {
    //TODO Se houver muitos casos de teste para um tipo de teste pode-se fazer o chamado testes parametrizados.
    @Mock
    private ClientRepository clientRepository;
    @Mock
    private ContactClientRepository contactClientRepository;

    private ClientService clientService;

    @BeforeEach
    public void setUp() {
        clientService = new ClientServiceImp(clientRepository, contactClientRepository);
    }

    @Test
    @DisplayName("Testa o serviço de armazenar um cliente")
    public void shouldSave() throws DontHaveTwoDistinctContactTypeException {
        //test 1
        //preparation
        Integer clientId = 1;
        RecordClientDto recordClientDto = ClientUtils.createRecordClientDto()
                                                     .build();
        Client client = RecordClientDto.clientDtoToClient(clientId, recordClientDto);
        //execution
        when(clientRepository.save(any(Client.class))).thenReturn(client);
        //execution and verification
        Optional<ClientDto> clientResult = clientService.save(recordClientDto);
        assertThat(clientResult.orElseThrow()
                               .getIdentificator(),
                   equalTo(client.getIdentificator()));
        assertThat(clientResult.orElseThrow()
                               .getAddress(),
                   equalTo(client.getAddress()));
        assertThat(clientResult.orElseThrow()
                               .getName(),
                   equalTo(client.getName()));
        assertThat(clientResult.orElseThrow()
                               .getBirthDate(),
                   equalTo(client.getBirthDate()));
        assertThat(clientResult.orElseThrow()
                               .getStatus(),
                   equalTo(client.getStatus()));
        assertThat(ClientDto.clientDtoToClient(clientResult.orElseThrow())
                            .getContactList(),
                   equalTo(client.getContactList()));
    }

    @Test
    @DisplayName("Testa o serviço de armazenar um cliente com apenas um tipo de contato na lista")
    public void shouldNotSaveWithContactListOfType() {
        //test 1
        //preparation
        RecordClientDto recordClientDto = ClientUtils.createRecordClientDtoWithoutDistinctContactType()
                                                     .build();
        //execution
        //execution and verification
        Assertions.assertThrows(DontHaveTwoDistinctContactTypeException.class, () ->
                clientService.save(recordClientDto)
        );

    }

    @Test
    @DisplayName("Testa o serviço de atualizar um cliente armazenado")
    public void shouldUpdate() throws DontHaveTwoDistinctContactTypeException, ClientNotFoundException {
        //test 1
        //preparation
        Integer clientId = 1;
        RecordClientDto recordClientDto = ClientUtils.createRecordClientDto()
                                                     .build();
        Client client = RecordClientDto.clientDtoToClient(clientId, recordClientDto);
        //execution
        when(clientRepository.findById(anyInt())).thenReturn(Optional.of(client));
        when(clientRepository.save(any(Client.class))).thenReturn(client);
        //execution and verification
        Optional<ClientDto> clientResult = clientService.update(clientId, recordClientDto);
        assertThat(clientResult.orElseThrow()
                               .getIdentificator(),
                equalTo(client.getIdentificator()));
        assertThat(clientResult.orElseThrow()
                               .getAddress(),
                equalTo(client.getAddress()));
        assertThat(clientResult.orElseThrow()
                               .getName(),
                equalTo(client.getName()));
        assertThat(clientResult.orElseThrow()
                               .getBirthDate(),
                equalTo(client.getBirthDate()));
        assertThat(clientResult.orElseThrow()
                               .getStatus(),
                equalTo(client.getStatus()));
        assertThat(ClientDto.clientDtoToClient(clientResult.orElseThrow())
                            .getContactList(),
                equalTo(client.getContactList()));
    }

    @Test
    @DisplayName("Testa o serviço de atualizar um cliente que não está armazenado")
    public void shouldNotUpdateWithContactListOfType() {
        //test 1
        //preparation
        Integer clientId = 1;
        RecordClientDto recordClientDto = ClientUtils.createRecordClientDtoWithoutDistinctContactType()
                                                     .build();
        //execution
        //execution and verification
        Assertions.assertThrows(DontHaveTwoDistinctContactTypeException.class, () ->
                clientService.update(clientId, recordClientDto)
        );
    }

    @Test
    @DisplayName("Testa o serviço de atualizar um cliente com apenas um tipo de contato na lista")
    public void thereIsNoSpecificClientToUpdate() {
        //test 1
        //preparation
        Integer clientId = 1;
        RecordClientDto recordClientDto = ClientUtils.createRecordClientDto()
                                                     .build();
        Client client = RecordClientDto.clientDtoToClient(clientId, recordClientDto);
        //execution
        //execution and verification
        Assertions.assertThrows(ClientNotFoundException.class, () ->
                clientService.update(clientId, recordClientDto)
        );
    }


    @Test
    @DisplayName("Testa o serviço de obter um cliente")
    public void shouldGet() throws ClientNotFoundException {
        //test 1
        //preparation
        ClientDto clientDto = ClientUtils.createClientDto()
                                         .build();
        Integer clientId = clientDto.getIdentificator();
        Client client = ClientDto.clientDtoToClient(clientDto);
        //execution
        when(clientRepository.findById(anyInt())).thenReturn(Optional.of(client));
        //execution and verification
        Optional<ClientDto> listClientResult = clientService.getByIdentificator(clientId);
        assertThat(listClientResult.orElseThrow()
                                   .getIdentificator(),
                equalTo(client.getIdentificator()));
        assertThat(listClientResult.orElseThrow()
                                   .getAddress(),
                equalTo(client.getAddress()));
        assertThat(listClientResult.orElseThrow()
                                   .getName(),
                equalTo(client.getName()));
        assertThat(listClientResult.orElseThrow()
                                   .getBirthDate(),
                equalTo(client.getBirthDate()));
        assertThat(listClientResult.orElseThrow()
                                   .getStatus(),
                equalTo(client.getStatus()));
        assertThat(ClientDto.clientDtoToClient(listClientResult.orElseThrow())
                            .getContactList(),
                equalTo(client.getContactList()));
    }

    @Test
    @DisplayName("Testa o serviço de obter um cliente que não existe")
    public void thereIsNoSpecificClientToGet() {
        //test 1
        //preparation
        Integer clientId = 1;
        //execution
        //execution and verification
        Assertions.assertThrows(ClientNotFoundException.class, () ->
                clientService.getByIdentificator(clientId)
        );
    }

    @Test
    @DisplayName("Testa o serviço de listar os clientes")
    public void shouldList() {
        //test 1
        //preparation
        ClientDto clientDto = ClientUtils.createClientDto()
                                         .build();
        Client client = ClientDto.clientDtoToClient(clientDto);
        List<Client> clientList = Collections.singletonList(client);
        //execution
        when(clientRepository.findAll()).thenReturn(clientList);
        //execution and verification
        List<ClientDto> listClientResult = clientService.listAll();
        assertThat(listClientResult.get(0)
                                   .getIdentificator(),
                   equalTo(client.getIdentificator()));
        assertThat(listClientResult.get(0)
                                   .getAddress(),
                   equalTo(client.getAddress()));
        assertThat(listClientResult.get(0)
                                   .getName(),
                   equalTo(client.getName()));
        assertThat(listClientResult.get(0)
                                   .getBirthDate(),
                   equalTo(client.getBirthDate()));
        assertThat(listClientResult.get(0)
                                   .getStatus(),
                   equalTo(client.getStatus()));
        assertThat(ClientDto.clientDtoToClient(listClientResult.get(0))
                            .getContactList(),
                   equalTo(client.getContactList()));
    }

    @Test
    @DisplayName("Testa o serviço de remover um cliente que não existe")
    public void shouldRemove() throws ClientNotFoundException {
        //test 1
        //preparation
        Integer clientId = 1;
        //execution
        //execution and verification
        clientService.remove(clientId);
    }

    @Test
    @DisplayName("Testa o serviço de remover um cliente que não existe")
    public void thereIsNoSpecificClientToRemove() {
        //test 1
        //preparation
        Integer clientId = 1;
        //execution
        doThrow(EmptyResultDataAccessException.class).when(clientRepository)
                                                     .deleteById(anyInt());
        //execution and verification
        Assertions.assertThrows(ClientNotFoundException.class, () ->
                clientService.remove(clientId)
        );
    }

}
