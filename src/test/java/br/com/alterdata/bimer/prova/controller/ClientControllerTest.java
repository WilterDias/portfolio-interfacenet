package br.com.alterdata.bimer.prova.controller;

import br.com.alterdata.bimer.prova.CoreControllerTest;
import br.com.alterdata.bimer.prova.dtos.client.ClientDto;
import br.com.alterdata.bimer.prova.dtos.client.RecordClientDto;
import br.com.alterdata.bimer.prova.modelo.RoleTypeEnum;
import br.com.alterdata.bimer.prova.repositorio.ClientRepository;
import br.com.alterdata.bimer.prova.servico.ClientService;
import br.com.alterdata.bimer.prova.utils.ClientUtils;
import br.com.alterdata.bimer.prova.utils.EnumConverterUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.http.MediaType;

import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;

@ExtendWith(MockitoExtension.class)
@WebMvcTest(ClientController.class)
@DisplayName("Valida o controller do cliente")
@ContextConfiguration(classes = {ClientController.class,
                                 ClientService.class,
                                 ClientRepository.class,
                                 EnumConverterUtils.class})
public class ClientControllerTest extends CoreControllerTest {

    @MockBean
    private ClientService clientService;

    private static final String CLIENT_URI="/api/clients";

    //TODO Se houver muitos casos de teste para um tipo de teste pode-se fazer o chamado testes parametrizados.
    @Test
    //@WithMockUser(username = "user", password = "user", roles = "USER")
    @DisplayName("Deve criar um cliente com suas propriedades")
    public void shouldSave() throws Exception {
        //test 1
        //preparation
        ClientDto clientDto = ClientUtils.createClientDto()
                                         .build();
        RecordClientDto recordClientDto = ClientUtils.createRecordClientDto()
                                                     .build();
        String clientDtoJson = new ObjectMapper().writeValueAsString(recordClientDto);
        //execution
        when(clientService.save(any(RecordClientDto.class))).thenReturn(Optional.of(clientDto));
        //execution and verification
        mockMvc.perform(post(CLIENT_URI)
               .contentType(MediaType.APPLICATION_JSON_VALUE)
               .content(clientDtoJson)
               .with(user(RoleTypeEnum.ROLE_USER.getLabel()))
               .with(csrf())
               .accept(MediaType.APPLICATION_JSON_VALUE))
               .andExpect(status().isCreated())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
               .andExpect(jsonPath("$.identificator", notNullValue()))
               .andExpect(jsonPath("$.name", is(clientDto.getName())))
               .andExpect(jsonPath("$.address", is(clientDto.getAddress())))
               .andExpect(jsonPath("$.birthDate", is(clientDto.getBirthDate()
                                                                       .format(DateTimeFormatter
                                                                               .ofPattern("dd-MM-yyyy")))))
               .andExpect(jsonPath("$.status", is(clientDto.getStatus())));

    }

    @Test
    //@WithMockUser(username = "user", password = "user", roles = "USER")
    @DisplayName("Deve remover um cliente com suas propriedades")
    public void shouldRemove() throws Exception {
        //test 1
        //preparation
        Integer clienteRemove = ClientUtils.createClientDto()
                                           .build()
                                           .getIdentificator();
        //execution
        //execution and verification
        mockMvc.perform(delete(CLIENT_URI + "/" + clienteRemove)
               .contentType(MediaType.APPLICATION_JSON_VALUE)
               .with(user(RoleTypeEnum.ROLE_USER.getLabel()))
               .with(csrf())
               .content(""))
               .andExpect(status().isNoContent());

    }

    @Test
    //@WithMockUser(username = "user", password = "user", roles = "USER")
    @DisplayName("Testa a atualização das propriedades de um cliente")
    public void shouldUpdate() throws Exception {
        //test 1
        //preparation
        ClientDto clientDto = ClientUtils.createClientDto().build();
        RecordClientDto recordClientDto = ClientUtils.createRecordClientDto().build();
        String clientDtoJson = new ObjectMapper().writeValueAsString(recordClientDto);
        //execution
        when(clientService.update(anyInt(), any(RecordClientDto.class))).thenReturn(Optional.of(clientDto));
        //execution and verification
        mockMvc.perform(put(CLIENT_URI + "/"+clientDto.getIdentificator())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(clientDtoJson)
                .with(user(RoleTypeEnum.ROLE_USER.getLabel()))
                .with(csrf())
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isAccepted())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.identificator", notNullValue()))
                .andExpect(jsonPath("$.name", is(clientDto.getName())))
                .andExpect(jsonPath("$.address", is(clientDto.getAddress())))
                .andExpect(jsonPath("$.birthDate", is(clientDto.getBirthDate()
                        .format(DateTimeFormatter
                                .ofPattern("dd-MM-yyyy")))))
                .andExpect(jsonPath("$.status", is(clientDto.getStatus())));
    }

    @Test
    //@WithMockUser(username = "user", password = "user", roles = "USER")
    @DisplayName("Testa a obtenção de um cliente")
    public void shouldGet() throws Exception {
        //test 1
        //preparation
        ClientDto clientDto = ClientUtils.createClientDto()
                                         .build();
        String clientDtoJson = new ObjectMapper().writeValueAsString(clientDto);
        //execution
        when(clientService.getByIdentificator(anyInt())).thenReturn(Optional.of(clientDto));
        //execution and verification
        mockMvc.perform(get(CLIENT_URI + "/" + clientDto.getIdentificator())
               .contentType(MediaType.APPLICATION_JSON_VALUE)
               .content(clientDtoJson)
               .with(user(RoleTypeEnum.ROLE_USER.getLabel()))
               .with(csrf())
               .accept(MediaType.APPLICATION_JSON_VALUE))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
               .andExpect(jsonPath("$.identificator", notNullValue()))
               .andExpect(jsonPath("$.name", is(clientDto.getName())))
               .andExpect(jsonPath("$.address", is(clientDto.getAddress())))
               .andExpect(jsonPath("$.birthDate", is(clientDto.getBirthDate()
                       .format(DateTimeFormatter
                               .ofPattern("dd-MM-yyyy")))))
               .andExpect(jsonPath("$.status", is(clientDto.getStatus())));
    }

    @Test
    //@WithMockUser(username = "user", password = "user", roles = "USER")
    @DisplayName("Testa a listagem de clientes")
    public void shouldList() throws Exception {
        //test 1
        //preparation
        ClientDto clientDto = ClientUtils.createClientDto()
                                         .build();
        List<ClientDto> listClientDto = Collections.singletonList(clientDto);
        RecordClientDto recordClientDto = ClientUtils.createRecordClientDto()
                                                     .build();
        String clientDtoJson = new ObjectMapper().writeValueAsString(recordClientDto);
        //execution
        when(clientService.listAll()).thenReturn(listClientDto);
        //execution and verification
        mockMvc.perform(get(CLIENT_URI)
               .contentType(MediaType.APPLICATION_JSON_VALUE)
               .content(clientDtoJson)
               .with(user(RoleTypeEnum.ROLE_USER.getLabel()))
               .with(csrf())
               .accept(MediaType.APPLICATION_JSON_VALUE))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
               .andExpect(jsonPath("$[0].identificator", notNullValue()))
               .andExpect(jsonPath("$[0].name", is(clientDto.getName())))
               .andExpect(jsonPath("$[0].address", is(clientDto.getAddress())))
               .andExpect(jsonPath("$[0].birthDate", is(clientDto.getBirthDate()
                                                                       .format(DateTimeFormatter
                                                                               .ofPattern("dd-MM-yyyy")))))
               .andExpect(jsonPath("$[0].status", is(clientDto.getStatus())));
    }

    @Test
    //@WithMockUser(username = "user", password = "user", roles = "USER")
    @DisplayName("Testa a pesquisa por nome e endereço para os clientes")
    public void shouldSearchNamePlusAddress() throws Exception {
        //test 1
        //preparation
        ClientDto clientDto = ClientUtils.createClientDto()
                                         .build();
        List<ClientDto> listClientDto = Collections.singletonList(clientDto);
        RecordClientDto recordClientDto = ClientUtils.createRecordClientDto()
                                                     .build();
        String clientDtoJson = new ObjectMapper().writeValueAsString(recordClientDto);
        //execution
        when(clientService.findByNamePlusAddress(anyString(), anyString(), anyString())).thenReturn(listClientDto);
        //execution and verification
        mockMvc.perform(get(CLIENT_URI + "/search?"+"logic=and"+"&name=wilter"+"&address=rua tal")
               .contentType(MediaType.APPLICATION_JSON_VALUE)
               .content(clientDtoJson)
               .with(user(RoleTypeEnum.ROLE_USER.getLabel()))
               .with(csrf())
               .accept(MediaType.APPLICATION_JSON_VALUE))
               .andExpect(status().isOk());
    }
}
